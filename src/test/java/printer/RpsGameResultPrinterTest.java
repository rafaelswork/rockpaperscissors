package printer;

import model.Player;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;

public class RpsGameResultPrinterTest {

    @Test
    public void printResults() {
        Player playerA = Mockito.mock(Player.class);
        Player playerB = Mockito.mock(Player.class);

        Mockito.when(playerA.getWinnings()).thenReturn(28);
        Mockito.when(playerB.getWinnings()).thenReturn(23);

        assertEquals("Results of 100 Games \n" +
                "Player A ---- ties ---- Player B\n" +
                "28 ----------  49  ---------- 23",
                new RpsGameResultPrinter().printResults(playerA, playerB));
    }
}