package util;


import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RpsUtilTest {

    @Test
    public void getRandomNumberInRangeZeroThree() {
        int min = 2;
        int max = 0;
        for (int i=0; i<100; i++) {
            int random = RpsUtil.getRandomNumberInRangeZeroThree();
            if (random < min) {
                min = random;
            }
            if (random > max) {
                max = random;
            }
        }
        assertEquals(0, min);
        assertEquals(2, max);
    }
}