package printer;

import model.Player;

public class RpsGameResultPrinter {
    public String printResults(Player playerA, Player playerB) {
        return "Results of 100 Games \n"
                + "Player A ---- ties ---- Player B\n"
                + String.valueOf(playerA.getWinnings())
                + " ----------  "
                + String.valueOf(100 - playerB.getWinnings() - playerA.getWinnings())
                + "  ---------- "
                + String.valueOf(playerB.getWinnings());
    }
}
