package model;

public class Rock extends AbstractSymbol {
    @Override
    public boolean beats(AbstractSymbol symbol) {
        return symbol.getClass().equals(Scissors.class);
    }
}
