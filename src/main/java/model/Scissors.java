package model;

public class Scissors extends AbstractSymbol{
    @Override
    public boolean beats(AbstractSymbol symbol) {
        return symbol.getClass().equals(Paper.class);
    }
}


