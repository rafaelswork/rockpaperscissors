package model;

public class Paper extends AbstractSymbol {
    @Override
    public boolean beats(AbstractSymbol symbol) {
        return symbol.getClass().equals(Rock.class);
    }
}
