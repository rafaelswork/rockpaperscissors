package model;

public abstract class AbstractSymbol {

    public abstract boolean beats(AbstractSymbol symbol);

}
