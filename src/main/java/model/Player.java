package model;

import service.SymbolService;

public class Player {
    private int winnings;
    private AbstractSymbol symbol;
    private boolean takesRandomSymbol;
    private SymbolService symbolService;

    public Player (SymbolService symbolService) {
        this.symbolService = symbolService;
        this.takesRandomSymbol = true;
    }

    public int getWinnings() {
        return winnings;
    }

    public void incrementWinnings() {
        winnings++;
    }

    public AbstractSymbol getSymbol() {
        if (takesRandomSymbol) {
            return symbolService.getRandomSymbol();
        }
        return symbol;
    }

    public void setSymbol(AbstractSymbol symbol) {
        this.symbol = symbol;
        takesRandomSymbol = false;
    }
}
