import model.Paper;
import model.Player;
import printer.RpsGameResultPrinter;
import rpsGame.RpsMainGame;
import rpsGame.RpsRound;
import service.SymbolService;

class RpsFactory {

    static RpsGameResultPrinter createRpsGameResultPrinter() {
        return new RpsGameResultPrinter();
    }

    static RpsMainGame createRpsMainGame() {
        return new RpsMainGame(createRpsRound());
    }

    private static RpsRound createRpsRound() {
        return new RpsRound();
    }

    static Player createPlayerPlayingRandomSymbols() {
        return new Player(createSymbolService());
    }

    static Player createPlayerPlayingAlwaysPaper() {
        Player player = new Player(null);
        player.setSymbol(new Paper());
        return player;
    }

    private static SymbolService createSymbolService() {
        return new SymbolService();
    }
}
