package service;

import model.AbstractSymbol;
import model.Paper;
import model.Rock;
import model.Scissors;
import util.RpsUtil;

public class SymbolService {
    public AbstractSymbol getRandomSymbol() {
        int randomNumber = RpsUtil.getRandomNumberInRangeZeroThree();
        if (randomNumber == 0) {
            return new Paper();
        }
        if (randomNumber == 1)
            return new Rock();
        return new Scissors();
    }
}
