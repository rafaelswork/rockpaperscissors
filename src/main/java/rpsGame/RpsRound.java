package rpsGame;

import model.Player;

public class RpsRound {

    void playRound(Player playerA, Player playerB) {
        if (playerA.getSymbol().beats(playerB.getSymbol())) {
            playerA.incrementWinnings();
        } else if (playerB.getSymbol().beats(playerA.getSymbol())) {
            playerB.incrementWinnings();
        }
    }
}
