package rpsGame;

import model.Player;

import java.util.stream.IntStream;

public class RpsMainGame {
    private final RpsRound rpsRound;

    public RpsMainGame (RpsRound rpsRound) {
        this.rpsRound = rpsRound;
    }

    public void startGame(Player playerA, Player playerB) {
        IntStream.range(1, 100).forEach(i -> rpsRound.playRound(playerA, playerB));
    }
}
