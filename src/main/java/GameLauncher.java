import model.Player;
import rpsGame.RpsMainGame;

import java.io.PrintStream;

public class GameLauncher {
    public static void main(String[] args) {
        RpsMainGame rpsGame = RpsFactory.createRpsMainGame();
        Player playerA = RpsFactory.createPlayerPlayingRandomSymbols();
        Player playerB = RpsFactory.createPlayerPlayingAlwaysPaper();
        rpsGame.startGame(playerA, playerB);
        System.out.println(RpsFactory.createRpsGameResultPrinter().printResults(playerA, playerB));
    }


}
